/*
           _   __ ______ __ __  ___     ____ __ __ ____     ______ _____ _____
          / | / // ____// //_/ /   |   /  _// //_// __ \   / ____// ___// ___/
         /  |/ // __/  / ,<   / /| |   / / / ,<  / / / /  / /     \__ \ \__ \ 
        / /|  // /___ / /| | / ___ | _/ / / /| |/ /_/ /  / /___  ___/ /___/ / 
       /_/ |_//_____//_/ |_|/_/  |_|/___//_/ |_|\____/   \____/ /____//____/  
         ______ ____   ___     __  ___ ______ _       __ ____   ____   __ __  
        / ____// __ \ /   |   /  |/  // ____/| |     / // __ \ / __ \ / //_/  
       / /_   / /_/ // /| |  / /|_/ // __/   | | /| / // / / // /_/ // ,<     
      / __/  / _, _// ___ | / /  / // /___   | |/ |/ // /_/ // _, _// /| |    
     /_/    /_/ |_|/_/  |_|/_/  /_//_____/   |__/|__/ \____//_/ |_|/_/ |_|    
             ______ __  __ __     ____   ______ ____ __     ______            
            / ____// / / // /    / __ \ / ____//  _// /    / ____/            
           / / __ / / / // /    / /_/ // /_    / / / /    / __/               
          / /_/ // /_/ // /___ / ____// __/  _/ / / /___ / /___               
          \____/ \____//_____//_/    /_/    /___//_____//_____/     
          
*/
'use strict';



//  REQUIREMENTS + FILE LOCATIONS

let gulp     = require('gulp');
let cleanCSS = require('gulp-clean-css');

let sass      = require('gulp-sass');
let sassFile  = './framework/sass/**/*.scss';
let cssOutput = './framework/css';
let cssPreMin = './framework/css/**/*.css';
let cssMin    = './framework/css-min/'

sass.compiler = require('node-sass');


//  SASS COMPILER

gulp.task('sass', function (done) {
  return gulp.src(sassFile)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(cssOutput));
    done();
});
 
 
//  CSS MINIFIER

gulp.task('minify-css', () => {
  return gulp.src(cssPreMin)
    .pipe(cleanCSS({debug: true}, (details) => {
      console.log(`${details.name}: ${details.stats.originalSize}`);
      console.log(`${details.name}: ${details.stats.minifiedSize}`);
    }))
  .pipe(gulp.dest(cssMin));
});


//  PUBLISH CODE

gulp.task('publish', gulp.parallel('sass', 'minify-css'));


// DEVELOPMENT WATCH TASK

gulp.task('default', function (done) {
  gulp.watch(sassFile, gulp.series('sass'));
  gulp.watch(cssPreMin, gulp.series('minify-css'));
  done()
});  

